import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductosComponent } from './productos/productos.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { GuardService } from './utilities/guard';

const routes: Routes = [
    { path: 'productos', component: ProductosComponent, canActivate: [GuardService] },
    { path: 'login', component: LoginComponent },
    { path: '',   component: HomeComponent },
    { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
