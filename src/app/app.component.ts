import { Component, OnInit, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService, User, Permiso } from './utilities/login.service';
import { StorageManagerService } from './utilities/storage-manager.service';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

    logged = false;
    user: User = null;
    permisos: Permiso[] = [];

    constructor(
        private loginService: LoginService,
        private storageManagerService: StorageManagerService,
        private router: Router,
        private cdr: ChangeDetectorRef
    ) { }

    ngOnInit(): void {
        if (this.loginService.checkLogged()) {
            this.user = this.storageManagerService.get('user');
            this.permisos = this.storageManagerService.get('permisos');
            this.logged = true;
        }
        this.loginService.isLogged.subscribe(
            (isLogged) => {
                this.logged = isLogged;
                if (isLogged) {
                    this.user = this.storageManagerService.get('user');
                    this.permisos = this.storageManagerService.get('permisos');
                    this.router.navigate(['/']);
                } else {
                    const currentUrl = this.router.url;
                    this.user = null;
                    this.permisos = [];
                    this.router.navigate(['/']).then(resolved => {
                        this.router.navigate([currentUrl]);
                    });
                }
            });
    }

    ngAfterViewInit(): void {
    }

    goto(path: string) {
        console.log(path);
        this.router.navigate([path]);
    }

    logout() {
        this.loginService.logout();
    }

}
