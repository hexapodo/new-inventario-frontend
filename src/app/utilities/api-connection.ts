import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { TokenManagerService } from './token-manager.service';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';
import { LoginService } from './login.service';


export class ApiConnection {

    url: string;

    private httpHeaders: HttpHeaders = new HttpHeaders();

    constructor(
        protected http: HttpClient,
        protected tokenManagerService: TokenManagerService,
        protected loginService: LoginService
    ) {
        this.url = environment.backendUrl;
    }

    protected getAll<T>(endpoint: string): Observable<T> {
        const headers: HttpHeaders = this.updateHeaders();
        return this.http.get<T>(this.url + endpoint, { headers }).pipe(
            catchError(err => {
                if (err.error.mensaje === 'Token is not valid' || err.error.mensaje === 'Token is required') {
                    this.loginService.logout();
                }
                return throwError(err);
            })
        );
    }

    protected getOne<T>(endpoint: string, id: string): Observable<T> {
        const headers: HttpHeaders = this.updateHeaders();
        return this.http.get<T>(this.url + endpoint + '/' + id, { headers }).pipe(
            catchError(err => {
                if (err.error.mensaje === 'Token is not valid' || err.error.mensaje === 'Token is required') {
                    this.loginService.logout();
                }
                return throwError(err);
            })
        );
    }

    protected post<T>(endpoint: string, payload: object): Observable<T> {
        const headers: HttpHeaders = this.updateHeaders();
        return this.http.post<T>(this.url + endpoint, payload, { headers }).pipe(
            catchError(err => {
                if (err.error.mensaje === 'Token is not valid' || err.error.mensaje === 'Token is required') {
                    this.loginService.logout();
                }
                return throwError(err);
            })
        );
    }

    protected put<T>(endpoint: string, id: string, payload: object): Observable<T> {
        const headers: HttpHeaders = this.updateHeaders();
        return this.http.put<T>(this.url + endpoint + '/' + id, payload, { headers }).pipe(
            catchError(err => {
                if (err.error.mensaje === 'Token is not valid' || err.error.mensaje === 'Token is required') {
                    this.loginService.logout();
                }
                return throwError(err);
            })
        );
    }

    protected delete(endpoint: string, entity: string): Observable<number> {
        const headers: HttpHeaders = this.updateHeaders();
        return this.http.delete<number>(this.url + endpoint + '/' + entity, { headers }).pipe(
            catchError(err => {
                if (err.error.mensaje === 'Token is not valid' || err.error.mensaje === 'Token is required') {
                    this.loginService.logout();
                }
                return throwError(err);
            })
        );
    }

    private updateHeaders(type: string = 'json'): HttpHeaders {
        const token: string = this.tokenManagerService.get();
        let headers: HttpHeaders = this.httpHeaders;
        switch (type) {
            case 'json':
                headers = this.httpHeaders.append('Content-Type', 'application/json');
                break;
            default:
                break;
        }
        if (token) {
            headers = this.httpHeaders.append(environment.tokenField, token);
        }
        return headers;
    }
}

export interface ApiServiceInterface {
    url: string;
    endpoint: string;
}
