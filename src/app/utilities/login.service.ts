import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { StorageManagerService } from './storage-manager.service';
import { TokenManagerService } from './token-manager.service';
import { LoginConnectionService } from './login-connection.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    private logged: Subject<boolean> = new Subject();
    private error: Subject<boolean> = new Subject();

    private statusLogged = false;

    constructor(
        private storageManagerService: StorageManagerService,
        private tokenManagerService: TokenManagerService,
        private loginConnectionService: LoginConnectionService,
        private router: Router
    ) { }

    get isLogged(): Observable<boolean> {
        return this.logged.asObservable();
    }

    get onError(): Observable<boolean> {
        return this.error.asObservable();
    }

    get getStatusLogged(): boolean {
        this.statusLogged = this.checkLogged();
        return this.statusLogged;
    }

    logout() {
        this.tokenManagerService.destroy();
        this.storageManagerService.destroy('user');
        this.storageManagerService.destroy('permisos');
        this.statusLogged = false;
        this.logged.next(false);
        this.router.navigate(['login']);

    }

    validateLogin(data: Login) {
        const userField = 'user';
        const permisosField = 'permisos';

        this.loginConnectionService.login(data).subscribe(
            (respond) => {
                const user: User = respond[userField];
                const permisos: Permiso[] = respond[permisosField];
                this.tokenManagerService.set(respond[environment.tokenField]);
                this.storageManagerService.set('user', user);
                this.storageManagerService.set('permisos', permisos);
                this.statusLogged = true;
                this.logged.next(true);
            },
            (error) => {
                this.statusLogged = false;
                this.logged.next(false);
                this.error.next(true);
            }
        );
    }

    checkLogged(): boolean {
        const token = this.tokenManagerService.get();
        const user = this.storageManagerService.get('user');
        const permisos = this.storageManagerService.get('permisos');
        return (token !== null && user !== null && permisos !== null);
    }
}

export interface Login{
    userName: string;
    password: string;
}

export interface LoginResponse {
    success: boolean;
    user: User;
    permisos: Permiso[];
    'x-inventario-token': string;
}

export interface User {
    name: string;
    email: string;
    phone: string;
}

export interface Permiso {
    id: number;
    code: string;
    permiso: string;
}
