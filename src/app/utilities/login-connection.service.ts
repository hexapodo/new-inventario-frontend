import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiServiceInterface } from '../utilities/api-connection';
import { TokenManagerService } from '../utilities/token-manager.service';
import { Login } from './login.service';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class LoginConnectionService implements ApiServiceInterface {
    endpoint = 'login';
    url: string;

    constructor(
        protected http: HttpClient,
        protected tokenManagerService: TokenManagerService
    ) {
        this.url = environment.backendUrl;
    }

    login(payload: Login) {
        return this.http.post(this.url + this.endpoint, payload);
    }

}
