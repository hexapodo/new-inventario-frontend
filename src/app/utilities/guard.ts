import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
    providedIn: 'root'
})
export class GuardService implements CanActivate {

    constructor(
        public loginService: LoginService, public router: Router
    ) { }

    canActivate(): boolean {
        if (!this.loginService.getStatusLogged) {
            this.loginService.logout();
            return false;
        }
        return true;
    }
}