import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TokenManagerService {

    tokenField: string;

    constructor() {
        this.tokenField = environment.tokenField;
    }

    get(): string {
        return localStorage.getItem(this.tokenField);
    }

    set(value: string): void {
        localStorage.setItem(this.tokenField, value);
    }

    destroy(): void {
        localStorage.removeItem(this.tokenField);
    }
}
