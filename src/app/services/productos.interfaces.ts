export interface Producto {
    id: number;
    nombre: string;
    activo: boolean;
    active: boolean;
    disabled: boolean;
    fotos: Foto[];
    presentaciones: Presentacion[];
    allFotos: Foto[];
}

export interface Foto {
    id: number;
    url: string;
}

export interface Presentacion {
    id: number;
    nombre: string;
    activo: boolean;
    cantidad: number;
    fotos: Foto[];
}
