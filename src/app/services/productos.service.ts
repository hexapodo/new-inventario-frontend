import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ApiConnection, ApiServiceInterface } from '../utilities/api-connection';
import { TokenManagerService } from '../utilities/token-manager.service';
import { Producto } from '../services/productos.interfaces';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { LoginService } from '../utilities/login.service';

@Injectable({
    providedIn: 'root'
})
export class ProductosService extends ApiConnection implements ApiServiceInterface {
    endpoint = 'productos';

    constructor(
        protected http: HttpClient,
        protected tokenManagerService: TokenManagerService,
        protected loginService: LoginService
    ) {
        super(http, tokenManagerService, loginService);
    }

    getProductos() {
        return super.getAll<Producto[]>(this.endpoint).pipe(
            map((elements) => {
                return elements.map((element) => {
                    let allFotos = element.fotos;
                    element.presentaciones.forEach(presentacion => {
                        allFotos = allFotos.concat(presentacion.fotos);
                    });
                    return {
                        ...element,
                        disabled: false,
                        active: false,
                        allFotos
                    };
                });
            })
        );
    }

}
