import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import { ProductoFrmComponent } from './producto-frm/producto-frm.component';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { ProductosService } from '../services/productos.service';
import { TokenManagerService } from '../utilities/token-manager.service';
import { Producto } from '../services/productos.interfaces';

@Component({
    selector: 'app-productos',
    templateUrl: './productos.component.html',
    styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {

    panels = [
        {
            active: true,
            name: 'This is panel header 1',
            disabled: false
        },
        {
            active: false,
            disabled: false,
            name: 'This is panel header 2'
        },
        {
            active: false,
            disabled: true,
            name: 'This is panel header 3'
        }
    ];

    array = [1, 2, 3, 4];

    effect = 'scrollx';

    productos: Producto[] = [];
    productosFiltrados: Producto[] = [];
    term: string;

    constructor(
        private drawerService: NzDrawerService,
        private modalService: NzModalService,
        private notificationService: NzNotificationService,
        protected tokenManagerService: TokenManagerService,
        private productosService: ProductosService
    ) { }

    ngOnInit(): void {
        this.productosService.getProductos().subscribe(
            (productos) => {
                this.productos = productos;
                this.productosFiltrados = productos;
            },
            (error) => {
                console.error(error);
            });
    }

    presentaciones(event: any) {
        event.stopPropagation();
    }

    editar(event: any) {
        event.stopPropagation();
    }

    borrar(event: any) {
        event.stopPropagation();
    }

    openComponent(event: Event): void {
        event.stopPropagation();
        const drawerRef = this.drawerService.create<ProductoFrmComponent, { value: string }, string>({
            nzTitle: 'Component',
            nzContent: ProductoFrmComponent,
            nzWidth: '50%',
            nzMaskClosable: false,
            nzKeyboard: false,
            nzContentParams: {
                value: 'jaja'
            }
        });

        drawerRef.afterOpen.subscribe(() => {
            console.log('Drawer(Component) open');
        });

        drawerRef.afterClose.subscribe(data => {
            console.log(data);
            if (typeof data === 'string') {
                // this.value = data;
            }
        });
    }

    showDeleteConfirm(event: Event): void {
        event.stopPropagation();
        this.modalService.confirm({
            nzTitle: 'Are you sure delete this task?',
            nzContent: '<b style="color: red;">Some descriptions</b>',
            nzOkText: 'Yes',
            nzOkType: 'danger',
            nzOnOk: this.deleteOk,
            nzCancelText: 'No',
            nzOnCancel: () => console.log('Cancel')
        });
    }

    deleteOk = () => {
        this.notificationService.success('Confirmación', 'Se ha borrado el registro');
    }

    onSearch(term: string) {
        this.productosFiltrados = this.productos.filter(producto => producto.nombre.toUpperCase().includes(term.trim().toUpperCase()));
        if (this.productosFiltrados.length === 1) {
            this.productosFiltrados[0].active = true;
        } else {
            this.productosFiltrados.forEach(producto => producto.active = false);
        }
    }
}
