import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductoFrmComponent } from './producto-frm.component';

describe('ProductoFrmComponent', () => {
  let component: ProductoFrmComponent;
  let fixture: ComponentFixture<ProductoFrmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductoFrmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductoFrmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
