import { Component, OnInit } from '@angular/core';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';

@Component({
    selector: 'app-producto-frm',
    templateUrl: './producto-frm.component.html',
    styleUrls: ['./producto-frm.component.scss']
})
export class ProductoFrmComponent implements OnInit {

    retorno = 'valor de retorno';
    value;

    constructor(
        private drawerRef: NzDrawerRef<string>
    ) { }

    ngOnInit(): void {
    }

    close(): void {
        this.drawerRef.close(this.retorno);
    }

}
