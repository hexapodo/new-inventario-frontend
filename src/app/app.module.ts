import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { es_ES } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzAffixModule } from 'ng-zorro-antd/affix';
import { IconDefinition } from '@ant-design/icons-angular';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzListModule } from 'ng-zorro-antd/list';

import {
    MenuFoldOutline,
    MenuOutline,
    MenuUnfoldOutline,
    CarOutline,
    CodepenOutline,
    TeamOutline,
    CarryOutOutline,
    DatabaseOutline,
    ShoppingCartOutline,
    SettingOutline,
    EditOutline,
    DeleteOutline,
    SearchOutline,
    AppstoreOutline,
    UserOutline,
    LockOutline,
    LoginOutline,
    LogoutOutline,
    HomeOutline
} from '@ant-design/icons-angular/icons';

import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ProductosComponent } from './productos/productos.component';
import { ProductoFrmComponent } from './productos/producto-frm/producto-frm.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

const icons: IconDefinition[] = [
    MenuFoldOutline,
    MenuOutline,
    MenuUnfoldOutline,
    CarOutline,
    CodepenOutline,
    TeamOutline,
    CarryOutOutline,
    DatabaseOutline,
    ShoppingCartOutline,
    SettingOutline,
    EditOutline,
    DeleteOutline,
    SearchOutline,
    AppstoreOutline,
    UserOutline,
    LockOutline,
    LoginOutline,
    LogoutOutline,
    HomeOutline
];

registerLocaleData(es);

@NgModule({
    declarations: [
        AppComponent,
        ProductosComponent,
        ProductoFrmComponent,
        HomeComponent,
        LoginComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        NzLayoutModule,
        NzBreadCrumbModule,
        NzMenuModule,
        NzAffixModule,
        NzCollapseModule,
        NzCarouselModule,
        NzButtonModule,
        NzToolTipModule,
        NzInputModule,
        NzDrawerModule,
        NzModalModule,
        NzNotificationModule,
        NzGridModule,
        NzFormModule,
        NzSpaceModule,
        NzPopoverModule,
        NzAvatarModule,
        NzListModule,
        NzIconModule.forRoot(icons),
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    ],
    providers: [{ provide: NZ_I18N, useValue: es_ES }],
    bootstrap: [AppComponent]
})
export class AppModule { }
