import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService, Login } from '../utilities/login.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

    validateForm!: FormGroup;
    validForm: boolean;
    private errorSubscription: Subscription;

    constructor(
        private fb: FormBuilder,
        private loginService: LoginService,
        private notificationService: NzNotificationService
    ) { }

    ngOnInit(): void {
        this.errorSubscription = this.loginService.onError.subscribe(
            (error) => {
                this.notificationService.error('Autenticación fallida', 'Usuario/Contraseña no válidos');
            }
        );
        this.validForm = false;
        this.validateForm = this.fb.group({
            username: [null, [Validators.required]],
            password: [null, [Validators.required]]
        });
    }

    ngOnDestroy(): void {
        if (this.errorSubscription) {
            this.errorSubscription.unsubscribe();
        }
    }

    submitForm(): void {
        // tslint:disable-next-line: forin
        for (const i in this.validateForm.controls) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
        }
        this.loginService.validateLogin(this.validateForm.value as Login);
    }

    updateState() {
        this.validForm = !this.validateForm.invalid;
    }

}
